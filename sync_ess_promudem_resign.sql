# 1 Resign

# Query Checking Resign

SELECT id,nip,firstname,resign_date FROM `sys_ttrs_employee`
where resign_date > now();
#------------------------------------------------------------------------------
UPDATE sys_ttrs_employee
SET sys_tmst_employee_type_id='11',last_employee_status=employee_status
WHERE 1
  and sys_tmst_employee_type_id=1
  and resign_date<>'0000-00-00'
  and resign_date IS NOT NULL
  and DATEDIFF(`resign_date`,NOW()) < 1
  
----check----
UPDATE sys_ttrs_employee
SET employee_status='4'
WHERE 1
	and employee_status <= 3
    and sys_tmst_employee_type_id=11
    and resign_date<>'0000-00-00'
    and resign_date IS NOT NULL
    and DATEDIFF(`resign_date`,NOW()) < 1
----check----
UPDATE sys_tmst_occupation_tree t, sys_ttrs_employee e SET t.sys_ttrs_employee_id='0'
WHERE e.sys_tmst_employee_type_id='11'
    and t.sys_ttrs_employee_id<>0
	and e.resign_date < CURDATE()
    and t.sys_ttrs_employee_id=e.id
----check----
UPDATE `sys_ttrs_employee` SET `last_employee_status` = `employee_status` WHERE (LENGTH(last_employee_status) = 0 OR `last_employee_status` IS NULL)
----check----

# 2 Promudem

# checking cronjob

select * FROM sys_ttrs_promutdem where
update_status = '0'
#and startdate > now();
# dan liat stardatenya lebih dari sekarang

eksekusinya diupdate yang tanggal skg dan sebelumnya.
dan merubah status menjadi update_status = '1'
------------------------------------------------------------------------------------------

UPDATE sys_tmst_occupation_tree,sys_ttrs_promutdem
SET sys_tmst_occupation_tree.sys_ttrs_employee_id = sys_ttrs_promutdem.sys_ttrs_employee_id
WHERE sys_tmst_occupation_tree.id = sys_ttrs_promutdem.sys_tmst_occupation_tree_id
AND sys_ttrs_promutdem.update_status = 0
AND sys_ttrs_promutdem.startdate <= CURDATE()
----check----
UPDATE sys_ttrs_employee,sys_ttrs_promutdem
SET sys_ttrs_employee.sys_tmst_unit_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_unit_id) > 0 OR sys_ttrs_promutdem.sys_tmst_unit_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_unit_id ELSE sys_ttrs_employee.sys_tmst_unit_id END,
sys_ttrs_employee.sys_tmst_level_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_level_id) > 0 OR sys_ttrs_promutdem.sys_tmst_level_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_level_id ELSE sys_ttrs_employee.sys_tmst_level_id END,
sys_ttrs_employee.sys_tmst_occupation_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_occupation_id) > 0 OR sys_ttrs_promutdem.sys_tmst_occupation_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_occupation_id ELSE sys_ttrs_employee.sys_tmst_occupation_id END,
sys_ttrs_employee.sys_tmst_grade_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_grade_id) > 0 OR sys_ttrs_promutdem.sys_tmst_grade_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_grade_id ELSE sys_ttrs_employee.sys_tmst_grade_id END,
sys_ttrs_employee.sys_tmst_department_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_department_id) > 0 OR sys_ttrs_promutdem.sys_tmst_department_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_department_id ELSE sys_ttrs_employee.sys_tmst_department_id END,
sys_ttrs_employee.sys_tmst_department_group_divisi_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_department_group_divisi_id) > 0 OR sys_ttrs_promutdem.sys_tmst_department_group_divisi_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_department_group_divisi_id ELSE sys_ttrs_employee.sys_tmst_department_group_divisi_id END,
sys_ttrs_employee.absen_id = CASE WHEN LENGTH(sys_ttrs_promutdem.absen_id) > 0 OR sys_ttrs_promutdem.absen_id IS NOT NULL THEN sys_ttrs_promutdem.absen_id ELSE sys_ttrs_employee.absen_id END,
sys_ttrs_employee.sys_tmst_company_npwp_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_company_npwp_id) > 0 OR sys_ttrs_promutdem.sys_tmst_company_npwp_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_company_npwp_id ELSE sys_ttrs_employee.sys_tmst_company_npwp_id END,
sys_ttrs_employee.sys_tmst_cost_center_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_cost_center_id) > 0 OR sys_ttrs_promutdem.sys_tmst_cost_center_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_cost_center_id ELSE sys_ttrs_employee.sys_tmst_cost_center_id END,
sys_ttrs_employee.sys_tmst_company_npp_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_company_npp_id) > 0 OR sys_ttrs_promutdem.sys_tmst_company_npp_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_company_npp_id ELSE sys_ttrs_employee.sys_tmst_company_npp_id END,
sys_ttrs_employee.custom1 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom1) > 0 OR sys_ttrs_promutdem.custom1 IS NOT NULL THEN sys_ttrs_promutdem.custom1 ELSE sys_ttrs_employee.custom1 END,
sys_ttrs_employee.custom2 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom2) > 0 OR sys_ttrs_promutdem.custom2 IS NOT NULL THEN sys_ttrs_promutdem.custom2 ELSE sys_ttrs_employee.custom2 END,
sys_ttrs_employee.custom3 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom3) > 0 OR sys_ttrs_promutdem.custom3 IS NOT NULL THEN sys_ttrs_promutdem.custom3 ELSE sys_ttrs_employee.custom3 END,
sys_ttrs_employee.custom4 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom4) > 0 OR sys_ttrs_promutdem.custom4 IS NOT NULL THEN sys_ttrs_promutdem.custom4 ELSE sys_ttrs_employee.custom4 END,
sys_ttrs_employee.custom5 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom5) > 0 OR sys_ttrs_promutdem.custom5 IS NOT NULL THEN sys_ttrs_promutdem.custom5 ELSE sys_ttrs_employee.custom5 END,
sys_ttrs_employee.custom6 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom6) > 0 OR sys_ttrs_promutdem.custom6 IS NOT NULL THEN sys_ttrs_promutdem.custom6 ELSE sys_ttrs_employee.custom6 END,
sys_ttrs_employee.custom7 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom7) > 0 OR sys_ttrs_promutdem.custom7 IS NOT NULL THEN sys_ttrs_promutdem.custom7 ELSE sys_ttrs_employee.custom7 END,
sys_ttrs_employee.custom8 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom8) > 0 OR sys_ttrs_promutdem.custom8 IS NOT NULL THEN sys_ttrs_promutdem.custom8 ELSE sys_ttrs_employee.custom8 END,
sys_ttrs_employee.custom9 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom9) > 0 OR sys_ttrs_promutdem.custom9 IS NOT NULL THEN sys_ttrs_promutdem.custom9 ELSE sys_ttrs_employee.custom9 END,
sys_ttrs_employee.custom10 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom10) > 0 OR sys_ttrs_promutdem.custom10 IS NOT NULL THEN sys_ttrs_promutdem.custom10 ELSE sys_ttrs_employee.custom10 END
WHERE sys_ttrs_employee.id = sys_ttrs_promutdem.sys_ttrs_employee_id
AND sys_ttrs_promutdem.update_status = 0
AND sys_ttrs_promutdem.startdate <= CURDATE()
----check----

UPDATE `sys_ttrs_promutdem` SET `update_status` = 1 WHERE CURDATE() >= startdate AND `update_status` = 0


x------batas company-----x
db_klola_dvl
----check----

# Query tiap 30 menit
# Grade Approval
	UPDATE `sys_ttrs_employee` SET `sys_ttrs_employee_group_id` =
	CASE sys_tmst_grade_id
		WHEN 1 THEN 1
		WHEN 2 THEN 1
	ELSE '2'
	END
	WHERE 1

----check----
	UPDATE `sys_tmst_ess_leave_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_leave_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=0,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id <> 3
	
----check----
	UPDATE `sys_tmst_ess_leave_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_leave_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=1,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id=3 and (d.cekitem=0 OR d.cekitem IS NULL)
	
----check----
	UPDATE `sys_tmst_ess_permit_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_permit_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=0,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id <> 3
	
----check----
	UPDATE `sys_tmst_ess_permit_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_permit_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=1,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id=3 and (d.cekitem=0 OR d.cekitem IS NULL)
	
----check----
	UPDATE `sys_tmst_ess_overtime_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_overtime_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=0,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id <> 3
	
----check----
	UPDATE `sys_tmst_ess_overtime_detail` d
	INNER JOIN sys_tmst_occupation_tree t ON t.id=d.sys_tmst_ess_overtime_id
	LEFT JOIN sys_ttrs_employee e ON e.id=t.sys_ttrs_employee_id
	SET d.cekitem=1,d.sys_tmst_grade_id='0'
	where e.sys_tmst_grade_id=3 and (d.cekitem=0 OR d.cekitem IS NULL)

#. 2 Clear spasi dan enter pada Pengajuan ESS
#  DO Sleep (15)

----check----
	UPDATE sys_ttrs_employee_ess_permit SET description = TRIM(REPLACE(REPLACE(REPLACE(`description`, '
', ' '), '
', ' '), '	', ' ')) where 1
	
----check----
	UPDATE sys_ttrs_employee_leave SET description = TRIM(REPLACE(REPLACE(REPLACE(`description`, '
', ' '), '
', ' '), '	', ' ')) where 1
	
----check----
	UPDATE sys_ttrs_employee_overtime SET description = TRIM(REPLACE(REPLACE(REPLACE(`description`, '
', ' '), '
', ' '), '	', ' ')) where 1
	
----check----
	UPDATE sys_ttrs_employee_overtime SET projectname = TRIM(REPLACE(REPLACE(REPLACE(`projectname`, '
', ' '), '
', ' '), '	', ' ')) where 1


#. 3 Resign	
----check----
UPDATE sys_ttrs_employee
SET sys_tmst_employee_type_id='11',last_employee_status=employee_status
WHERE 1
  and sys_tmst_employee_type_id=1
  and resign_date<>'0000-00-00'
  and resign_date IS NOT NULL
  and DATEDIFF(`resign_date`,NOW()) < 1
----check----
UPDATE sys_ttrs_employee
SET employee_status='4'
WHERE 1
	and employee_status <= 3
    and sys_tmst_employee_type_id=11
    and resign_date<>'0000-00-00'
    and resign_date IS NOT NULL
    and DATEDIFF(`resign_date`,NOW()) < 1
----check----
UPDATE sys_tmst_occupation_tree t, sys_ttrs_employee e SET t.sys_ttrs_employee_id='0'
WHERE e.sys_tmst_employee_type_id='11'
    and t.sys_ttrs_employee_id<>0
	and e.resign_date < CURDATE()
    and t.sys_ttrs_employee_id=e.id
----check----
UPDATE `sys_ttrs_employee` SET `last_employee_status` = `employee_status` WHERE (LENGTH(last_employee_status) = 0 OR `last_employee_status` IS NULL)
----check----

#. Promudem
UPDATE sys_tmst_occupation_tree,sys_ttrs_promutdem
SET sys_tmst_occupation_tree.sys_ttrs_employee_id = sys_ttrs_promutdem.sys_ttrs_employee_id
WHERE sys_tmst_occupation_tree.id = sys_ttrs_promutdem.sys_tmst_occupation_tree_id
AND sys_ttrs_promutdem.update_status = 0
AND sys_ttrs_promutdem.startdate <= CURDATE()
----check----
UPDATE sys_ttrs_employee,sys_ttrs_promutdem
SET sys_ttrs_employee.sys_tmst_unit_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_unit_id) > 0 OR sys_ttrs_promutdem.sys_tmst_unit_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_unit_id ELSE sys_ttrs_employee.sys_tmst_unit_id END,
sys_ttrs_employee.sys_tmst_level_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_level_id) > 0 OR sys_ttrs_promutdem.sys_tmst_level_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_level_id ELSE sys_ttrs_employee.sys_tmst_level_id END,
sys_ttrs_employee.sys_tmst_occupation_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_occupation_id) > 0 OR sys_ttrs_promutdem.sys_tmst_occupation_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_occupation_id ELSE sys_ttrs_employee.sys_tmst_occupation_id END,
sys_ttrs_employee.sys_tmst_grade_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_grade_id) > 0 OR sys_ttrs_promutdem.sys_tmst_grade_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_grade_id ELSE sys_ttrs_employee.sys_tmst_grade_id END,
sys_ttrs_employee.sys_tmst_department_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_department_id) > 0 OR sys_ttrs_promutdem.sys_tmst_department_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_department_id ELSE sys_ttrs_employee.sys_tmst_department_id END,
sys_ttrs_employee.sys_tmst_department_group_divisi_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_department_group_divisi_id) > 0 OR sys_ttrs_promutdem.sys_tmst_department_group_divisi_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_department_group_divisi_id ELSE sys_ttrs_employee.sys_tmst_department_group_divisi_id END,
sys_ttrs_employee.absen_id = CASE WHEN LENGTH(sys_ttrs_promutdem.absen_id) > 0 OR sys_ttrs_promutdem.absen_id IS NOT NULL THEN sys_ttrs_promutdem.absen_id ELSE sys_ttrs_employee.absen_id END,
sys_ttrs_employee.sys_tmst_company_npwp_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_company_npwp_id) > 0 OR sys_ttrs_promutdem.sys_tmst_company_npwp_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_company_npwp_id ELSE sys_ttrs_employee.sys_tmst_company_npwp_id END,
sys_ttrs_employee.sys_tmst_cost_center_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_cost_center_id) > 0 OR sys_ttrs_promutdem.sys_tmst_cost_center_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_cost_center_id ELSE sys_ttrs_employee.sys_tmst_cost_center_id END,
sys_ttrs_employee.sys_tmst_company_npp_id = CASE WHEN LENGTH(sys_ttrs_promutdem.sys_tmst_company_npp_id) > 0 OR sys_ttrs_promutdem.sys_tmst_company_npp_id IS NOT NULL THEN sys_ttrs_promutdem.sys_tmst_company_npp_id ELSE sys_ttrs_employee.sys_tmst_company_npp_id END,
sys_ttrs_employee.custom1 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom1) > 0 OR sys_ttrs_promutdem.custom1 IS NOT NULL THEN sys_ttrs_promutdem.custom1 ELSE sys_ttrs_employee.custom1 END,
sys_ttrs_employee.custom2 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom2) > 0 OR sys_ttrs_promutdem.custom2 IS NOT NULL THEN sys_ttrs_promutdem.custom2 ELSE sys_ttrs_employee.custom2 END,
sys_ttrs_employee.custom3 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom3) > 0 OR sys_ttrs_promutdem.custom3 IS NOT NULL THEN sys_ttrs_promutdem.custom3 ELSE sys_ttrs_employee.custom3 END,
sys_ttrs_employee.custom4 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom4) > 0 OR sys_ttrs_promutdem.custom4 IS NOT NULL THEN sys_ttrs_promutdem.custom4 ELSE sys_ttrs_employee.custom4 END,
sys_ttrs_employee.custom5 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom5) > 0 OR sys_ttrs_promutdem.custom5 IS NOT NULL THEN sys_ttrs_promutdem.custom5 ELSE sys_ttrs_employee.custom5 END,
sys_ttrs_employee.custom6 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom6) > 0 OR sys_ttrs_promutdem.custom6 IS NOT NULL THEN sys_ttrs_promutdem.custom6 ELSE sys_ttrs_employee.custom6 END,
sys_ttrs_employee.custom7 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom7) > 0 OR sys_ttrs_promutdem.custom7 IS NOT NULL THEN sys_ttrs_promutdem.custom7 ELSE sys_ttrs_employee.custom7 END,
sys_ttrs_employee.custom8 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom8) > 0 OR sys_ttrs_promutdem.custom8 IS NOT NULL THEN sys_ttrs_promutdem.custom8 ELSE sys_ttrs_employee.custom8 END,
sys_ttrs_employee.custom9 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom9) > 0 OR sys_ttrs_promutdem.custom9 IS NOT NULL THEN sys_ttrs_promutdem.custom9 ELSE sys_ttrs_employee.custom9 END,
sys_ttrs_employee.custom10 = CASE WHEN LENGTH(sys_ttrs_promutdem.custom10) > 0 OR sys_ttrs_promutdem.custom10 IS NOT NULL THEN sys_ttrs_promutdem.custom10 ELSE sys_ttrs_employee.custom10 END
WHERE sys_ttrs_employee.id = sys_ttrs_promutdem.sys_ttrs_employee_id
AND sys_ttrs_promutdem.update_status = 0
AND sys_ttrs_promutdem.startdate <= CURDATE()
----check----
UPDATE `sys_ttrs_promutdem` SET `update_status` = 1 WHERE CURDATE() >= startdate AND `update_status` = 0
