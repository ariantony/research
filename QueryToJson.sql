SELECT
  JSON_ARRAYAGG(JSON_OBJECT(
		'id', e.id,
		'name', e.firstname,
		'sys_ttrs_employee_fam_structure',fam.xx, #fam > untuk table alias atau key, xx > untuk alias field atau value
		'sys_ttrs_employee_work_exp',exp.yy
	)) as json_value
FROM
    sys_ttrs_employee e
    INNER JOIN (
        SELECT 
            sys_ttrs_employee_id, 
            JSON_ARRAYAGG(JSON_OBJECT(
				'id', fs.id,
				'fam_relation', tf.`name`,
				'nik', fs.nik,
				'kknik', fs.kknik,
				'bpjs_member', fs.bpjs_member,
				'bpjs_num', fs.bpjs_num,
				'name', fs.`name`,
				'sex', fs.sex,
				'dateofborn', fs.dateofborn,
				'last_education', fs.last_education,
				'last_occupation', fs.last_occupation,
				'description', fs.description,
				'marital_status', fs.marital_status,
				'tanggunganpajak', fs.tanggunganpajak,
				'workin_curr_company', fs.workin_curr_company
			)) xx 
        FROM sys_ttrs_employee_fam_structure fs
			  INNER JOIN sys_tmst_family tf ON tf.id=fs.fam_relation
        GROUP BY sys_ttrs_employee_id
    ) fam ON fam.sys_ttrs_employee_id = e.id
		INNER JOIN (
        SELECT 
            sys_ttrs_employee_id, 
            JSON_ARRAYAGG(JSON_OBJECT(
				'id',id,
				'startdate',startdate,
				'enddate',enddate,
				'company',company,
				'bussiness_line',bussiness_line,
				'occupation',occupation,
				'resign_reason',resign_reason,
				'wage',wage,
				'ref_name',ref_name,
				'ref_occupation',ref_occupation,
				'ref_phone',ref_phone,
				'description',description
		)) yy 
        FROM sys_ttrs_employee_work_exp 
        GROUP BY sys_ttrs_employee_id
    ) exp ON exp.sys_ttrs_employee_id = e.id
WHERE e.id='15090101331'