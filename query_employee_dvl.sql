SELECT
	floor( rand() * 1000000000 ) AS `keyid`,
	`e`.`id` AS `id`,
	`e`.`nama` AS `nama`,
	`e`.`nama_depan` AS `nama_depan`,
	`e`.`nama_tengah` AS `nama_tengah`,
	`e`.`nama_belakang` AS `nama_belakang`,
	`e`.`nama_panggilan` AS `nama_panggilan`,
	`e`.`reporting_line_id` AS `reporting_line_id`,
	`e`.`reporting_line_name` AS `reporting_line_name`,
	`e`.`divisi` AS `divisi`,
	`e`.`departemen` AS `departemen`,
	`e`.`seksi` AS `seksi`,
	`e`.`jabatan` AS `jabatan`,
	`e`.`kode_cost_center` AS `kode_cost_center`,
	`e`.`nama_cost_center` AS `nama_cost_center`,
	`e`.`area_kerja` AS `area_kerja`,
	`e`.`spv_level` AS `spv_level`,
	`e`.`golongan` AS `golongan`,
	`e`.`pangkat` AS `pangkat`,
	`e`.`kota_recruitmen` AS `kota_recruitmen`,
	`e`.`lokasi_kerja` AS `lokasi_kerja`,
	`e`.`tgl_mulai_kerja` AS `tgl_mulai_kerja`,
	`e`.`tgl_pegawai_tetap` AS `tgl_pegawai_tetap`,
	`e`.`tgl_resign` AS `tgl_resign`,
	`e`.`alasan_keluar` AS `alasan_keluar`,
	`e`.`status_pegawai` AS `status_pegawai`,
	`e`.`status_aktif/nonaktif` AS `status_aktif/nonaktif`,
	`e`.`pendidikan_terakhir` AS `pendidikan_terakhir`,
	`e`.`tempat_lahir` AS `tempat_lahir`,
	`e`.`tanggal_lahir` AS `tanggal_lahir`,
	`e`.`jenis_kelamin` AS `jenis_kelamin`,
	`e`.`status_perkawinan` AS `status_perkawinan`,
	`e`.`tanggal_pernikahan` AS `tanggal_pernikahan`,
	`e`.`kewarganegaraan` AS `kewarganegaraan`,
	`e`.`jenis_identitas` AS `jenis_identitas`,
	`e`.`nomor_identitas` AS `nomor_identitas`,
	`e`.`kadaluarsa_identitas` AS `kadaluarsa_identitas`,
	`e`.`nomor_kk` AS `nomor_kk`,
	`e`.`nik_kk` AS `nik_kk`,
	`e`.`tanggal_dikeluarkan_KK` AS `tanggal_dikeluarkan_KK`,
	`e`.`npwp_pegawai` AS `npwp_pegawai`,
	`e`.`nomor_jamsostek` AS `nomor_jamsostek`,
	`e`.`nomor_bpjs_kesehatan` AS `nomor_bpjs_kesehatan`,
	`e`.`kawin_pajak` AS `kawin_pajak`,
	`e`.`kpp` AS `kpp`,
	`e`.`npp_perusahaan` AS `npp_perusahaan`,
	`e`.`npwp_perusahaan` AS `npwp_perusahaan`,
	`e`.`nama_pasangan` AS `nama_pasangan`,
	`e`.`npwp_pasangan` AS `npwp_pasangan`,
	`e`.`negara` AS `negara`,
	`e`.`tgl_ke_indonesia` AS `tgl_ke_indonesia`,
	`e`.`nama_ibu_kandung` AS `nama_ibu_kandung`,
	`e`.`golongan_darah` AS `golongan_darah`,
	`e`.`agama` AS `agama`,
	`e`.`berat_badan` AS `berat_badan`,
	`e`.`tinggi_badan` AS `tinggi_badan`,
	`e`.`domisili_propinsi` AS `domisili_propinsi`,
	`e`.`domisili_kabupaten` AS `domisili_kabupaten`,
	`e`.`domisili_kecamatan` AS `domisili_kecamatan`,
	`e`.`domisili_kelurahan` AS `domisili_kelurahan`,
	`e`.`domisili_rt` AS `domisili_rt`,
	`e`.`domisili_rw` AS `domisili_rw`,
	`e`.`domisili_alamat` AS `domisili_alamat`,
	`e`.`domisili_kodepos` AS `domisili_kodepos`,
	`e`.`propinsi` AS `propinsi`,
	`e`.`kabupaten` AS `kabupaten`,
	`e`.`kecamatan` AS `kecamatan`,
	`e`.`kelurahan` AS `kelurahan`,
	`e`.`rt` AS `rt`,
	`e`.`rw` AS `rw`,
	`e`.`alamat` AS `alamat`,
	`e`.`kodepos` AS `kodepos`,
	`e`.`alamat_npwp` AS `alamat_npwp`,
	`e`.`telepon_rumah` AS `telepon_rumah`,
	`e`.`telepon_kantor` AS `telepon_kantor`,
	`e`.`hp1` AS `hp1`,
	`e`.`hp2` AS `hp2`,
	`e`.`email_kantor` AS `email_kantor`,
	`e`.`username` AS `username`,
	`e`.`email_pribadi` AS `email_pribadi`,
	`e`.`faskes_umum` AS `faskes_umum`,
	`e`.`faskes_gigi` AS `faskes_gigi`,
	`e`.`kode_bank` AS `kode_bank`,
	`e`.`nama_bank` AS `nama_bank`,
	`e`.`nomor_rekening` AS `nomor_rekening`,
	`e`.`atas_nama_rekening` AS `atas_nama_rekening`,
	`e`.`cabang` AS `cabang`,
	`e`.`kode_jurnal` AS `kode_jurnal`,
	`e`.`fb` AS `fb`,
	`e`.`ym` AS `ym`,
	`e`.`wa` AS `wa`,
	`e`.`tw` AS `tw`,
	`e`.`blog` AS `blog`,
	`e`.`hangout` AS `hangout`,
	`e`.`hobby` AS `hobby`,
	`e`.`tgl_efektif` AS `last_update`,
	`e`.`passport_num` AS `nomor_passport`,
	`e`.`passport_expired` AS `tanggal_berakhir_passport`,
	`e`.`sys_tmst_unit_id` AS `sys_tmst_unit_id`,
	`e`.`sys_tmst_grade_id` AS `sys_tmst_grade_id`,
	`e`.`sys_tmst_level_id` AS `sys_tmst_level_id`,
	`e`.`sys_tmst_company_id` AS `sys_tmst_company_id`,
	`e`.`sys_tmst_country_id` AS `sys_tmst_country_id`,
	`e`.`sys_ttrs_employee_id` AS `sys_ttrs_employee_id` 
FROM
	(
	SELECT
		`ee`.`nip` AS `id`,
		`ee`.`firstname` AS `nama`,
		`ee`.`custom11` AS `nama_depan`,
		`ee`.`custom12` AS `nama_tengah`,
		`ee`.`custom13` AS `nama_belakang`,
		`ee`.`surname` AS `nama_panggilan`,
	CASE
			
			WHEN ( `ud`.`upline_nip` = '' OR `ud`.`upline_nip` IS NULL ) THEN
		CASE
				
				WHEN ( `ud`.`upline2_nip` = '' OR `ud`.`upline2_nip` IS NULL ) THEN
				`ud`.`upline3_nip` ELSE `ud`.`upline2_nip` 
			END ELSE `ud`.`upline_nip` 
		END AS `reporting_line_id`,
	CASE
			
			WHEN ( `ud`.`upline_nip` = '' OR `ud`.`upline_nip` IS NULL ) THEN
		CASE
				
				WHEN ( `ud`.`upline2_nip` = '' OR `ud`.`upline2_nip` IS NULL ) THEN
				`ud`.`upline3_name` ELSE `ud`.`upline2_name` 
			END ELSE `ud`.`upline_name` 
		END AS `reporting_line_name`,
		`sd`.`name` AS `divisi`,
		`sd1`.`name` AS `departemen`,
	CASE
			
			WHEN substr( `de`.`departement_id`, 1, 9 ) THEN
			`de`.`name` ELSE '' 
		END AS `seksi`,
		`oc`.`alias` AS `jabatan`,
		`ee`.`sys_tmst_cost_center_id` AS `kode_cost_center`,
		`cc`.`name` AS `nama_cost_center`,
		`un`.`name` AS `area_kerja`,
		`ec2`.`name` AS `spv_level`,
		`lev`.`name` AS `golongan`,
		`gra`.`name` AS `pangkat`,
		concat( `ec1`.`code`, ' - ', `ec1`.`name` ) AS `kota_recruitmen`,
		`ec7`.`description` AS `lokasi_kerja`,
		`ee`.`startwork` AS `tgl_mulai_kerja`,
		`pd`.`permanent_date` AS `tgl_pegawai_tetap`,
		`ms`.`startdate` AS `tgl_efektif`,
		`ee`.`resign_date` AS `tgl_resign`,
		`phk`.`name` AS `alasan_keluar`,
	CASE
			
			WHEN `ee`.`employee_status` = '1' THEN
			'Percobaan' 
			WHEN `ee`.`employee_status` = '2' THEN
			'Kontrak' 
			WHEN `ee`.`employee_status` = '3' THEN
			'Tetap' 
			WHEN `ee`.`employee_status` = '4' THEN
		CASE
				
				WHEN `ee`.`last_employee_status` = '1' THEN
				'Percobaan' 
				WHEN `ee`.`last_employee_status` = '2' THEN
				'Kontrak' 
				WHEN `ee`.`last_employee_status` = '3' THEN
				'Tetap' 
			END 
			END AS `status_pegawai`,
		CASE
				
				WHEN ( `ee`.`employee_status` = 4 AND to_days( `ee`.`resign_date` ) - to_days( CURRENT_TIMESTAMP ()) < 0 ) THEN
				'Non Aktif' ELSE 'Aktif' 
			END AS `status_aktif/nonaktif`,
			`edu`.`name` AS `pendidikan_terakhir`,
			`ee`.`placeofborn` AS `tempat_lahir`,
			`ee`.`dateofborn` AS `tanggal_lahir`,
		CASE
				
				WHEN `ee`.`sex` = '1' THEN
				'L' ELSE 'P' 
			END AS `jenis_kelamin`,
			`mar`.`name` AS `status_perkawinan`,
			`ee`.`married_date` AS `tanggal_pernikahan`,
		CASE
				
				WHEN `ee`.`sys_tmst_country_id` = '360' THEN
				'WNI' ELSE 'WNA' 
			END AS `kewarganegaraan`,
			`typeid`.`name` AS `jenis_identitas`,
			`ee`.`identity_number` AS `nomor_identitas`,
		CASE
				
				WHEN `ee`.`custom14` = '1' THEN
				'_SEUMURHIDUP' ELSE `ee`.`custom5` 
			END AS `kadaluarsa_identitas`,
			`ee`.`kk_num` AS `nomor_kk`,
			`ee`.`custom3` AS `nik_kk`,
			`ee`.`custom6` AS `tanggal_dikeluarkan_KK`,
			`ee`.`tax_npwp_personal` AS `npwp_pegawai`,
			`ee`.`jamsostek_code` AS `nomor_jamsostek`,
			`ee`.`bpjs_num` AS `nomor_bpjs_kesehatan`,
			`ee`.`marital_status_tax` AS `kawin_pajak`,
			`kpp`.`name` AS `kpp`,
			`cnpp`.`name` AS `npp_perusahaan`,
			`cnpwp`.`npwp_number` AS `npwp_perusahaan`,
			`ee`.`couple_name` AS `nama_pasangan`,
			`ee`.`tax_npwp_couple` AS `npwp_pasangan`,
			`co`.`name` AS `negara`,
			`ee`.`startincountry` AS `tgl_ke_indonesia`,
			`fam`.`name` AS `nama_ibu_kandung`,
			`blood`.`name` AS `golongan_darah`,
			`rel`.`name` AS `agama`,
			`ee`.`weight_body` AS `berat_badan`,
			`ee`.`tall_body` AS `tinggi_badan`,
			`lvprov`.`name` AS `domisili_propinsi`,
			`lvgency`.`name` AS `domisili_kabupaten`,
			`lvdist`.`name` AS `domisili_kecamatan`,
			`ee`.`lv_kelurahan` AS `domisili_kelurahan`,
			`ee`.`lv_rt` AS `domisili_rt`,
			`ee`.`lv_rw` AS `domisili_rw`,
			`ee`.`lv_address` AS `domisili_alamat`,
			`ee`.`lv_postcode` AS `domisili_kodepos`,
			`idprov`.`name` AS `propinsi`,
			`idgency`.`name` AS `kabupaten`,
			`iddist`.`name` AS `kecamatan`,
			`ee`.`id_kelurahan` AS `kelurahan`,
			`ee`.`id_rt` AS `rt`,
			`ee`.`id_rw` AS `rw`,
			`ee`.`id_address` AS `alamat`,
			`ee`.`id_postcode` AS `kodepos`,
			`ee`.`custom4` AS `alamat_npwp`,
			`ee`.`homephone` AS `telepon_rumah`,
			`ee`.`officephone` AS `telepon_kantor`,
			`ee`.`mobile1` AS `hp1`,
			`ee`.`mobile2` AS `hp2`,
		CASE
				
				WHEN `ee`.`sys_tmst_employee_type_id` = '11' THEN
				'' ELSE `ee`.`mail_office` 
			END AS `email_kantor`,
		CASE
				
				WHEN `ee`.`sys_tmst_employee_type_id` = '11' THEN
				'' ELSE `ee`.`username` 
			END AS `username`,
		CASE
				
				WHEN `ee`.`sys_tmst_employee_type_id` = '11' THEN
				'' ELSE `ee`.`mail_personal` 
			END AS `email_pribadi`,
			`ee`.`custom9` AS `faskes_umum`,
			`ee`.`custom10` AS `faskes_gigi`,
			`rek`.`sys_tmst_bank_id` AS `kode_bank`,
			`bank`.`name` AS `nama_bank`,
			`rek`.`account_no` AS `nomor_rekening`,
			`rek`.`account_name` AS `atas_nama_rekening`,
			`rek`.`branch` AS `cabang`,
			`ee`.`journal_code` AS `kode_jurnal`,
			`ee`.`fb` AS `fb`,
			`ee`.`ym` AS `ym`,
			`ee`.`whatsup` AS `wa`,
			`ee`.`twitter` AS `tw`,
			`ee`.`blog` AS `blog`,
			`ee`.`hangout` AS `hangout`,
			`ee`.`hobby` AS `hobby`,
			`ee`.`passport_num` AS `passport_num`,
			`ee`.`passport_expired` AS `passport_expired`,
			`ee`.`sys_tmst_unit_id` AS `sys_tmst_unit_id`,
			`ee`.`sys_tmst_grade_id` AS `sys_tmst_grade_id`,
			`ee`.`sys_tmst_level_id` AS `sys_tmst_level_id`,
			`ee`.`sys_tmst_company_id` AS `sys_tmst_company_id`,
			`ee`.`sys_tmst_country_id` AS `sys_tmst_country_id`,
			`ee`.`id` AS `sys_ttrs_employee_id` 
		FROM
			((((((((((((((((((((((((((((((((((
																																					`db_klola_dvl`.`sys_ttrs_employee` `ee`
																																					LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom2` `ec` ON ( `ee`.`custom2` = `ec`.`id` ))
																																				LEFT JOIN `db_klola_dvl`.`sys_tmst_unit` `un` ON ( `ee`.`sys_tmst_unit_id` = `un`.`id` ))
																																			LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom1` `ec1` ON ( `ee`.`custom1` = `ec1`.`id` ))
																																		LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom2` `ec2` ON ( `ee`.`custom2` = `ec2`.`id` ))
																																	LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom7` `ec7` ON ( `ee`.`custom7` = `ec7`.`id` ))
																																LEFT JOIN `db_klola_dvl`.`sys_tmst_level` `lev` ON ( `ee`.`sys_tmst_level_id` = `lev`.`id` ))
																															LEFT JOIN `db_klola_dvl`.`sys_tmst_grade` `gra` ON ( `ee`.`sys_tmst_grade_id` = `gra`.`id` ))
																														LEFT JOIN `db_klola`.`sys_tmst_country` `co` ON ( `ee`.`sys_tmst_country_id` = `co`.`id` ))
																													LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `de` ON ( `ee`.`sys_tmst_department_id` = `de`.`id` ))
																												LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `sd` ON (
																												`sd`.`departement_id` = substr( `de`.`departement_id`, 1, 2 )))
																											LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `sd1` ON (
																											`sd1`.`departement_id` = substr( `de`.`departement_id`, 1, 5 )))
																										LEFT JOIN `db_klola_dvl`.`sys_tmst_occupation` `oc` ON ( `ee`.`sys_tmst_occupation_id` = `oc`.`id` ))
																									LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_upline_detail` `ud` ON ( `ud`.`id` = `ee`.`id` ))
																								LEFT JOIN `db_klola_dvl`.`sys_tmst_cost_center` `cc` ON ( `cc`.`code` = `ee`.`sys_tmst_cost_center_id` ))
																							LEFT JOIN `db_klola`.`sys_tmst_marital_status` `mar` ON ( `mar`.`id` = `ee`.`sys_tmst_marital_status_id` ))
																						LEFT JOIN `db_klola_dvl`.`sys_tmst_identity_type` `typeid` ON ( `typeid`.`id` = `ee`.`sys_tmst_identity_type_id` ))
																					LEFT JOIN `db_klola`.`sys_tmst_kpp_code` `kpp` ON ( `kpp`.`id` = `ee`.`sys_tmst_kpp_id` ))
																				LEFT JOIN `db_klola_dvl`.`sys_tmst_company_npwp` `cnpwp` ON ( `cnpwp`.`id` = `ee`.`sys_tmst_company_npwp_id` ))
																			LEFT JOIN `db_klola_dvl`.`sys_tmst_company_npp` `cnpp` ON ( `cnpp`.`id` = `ee`.`sys_tmst_company_npp_id` ))
																		LEFT JOIN `db_klola_dvl`.`sys_tmst_blood_type` `blood` ON ( `blood`.`id` = `ee`.`sys_tmst_blood_type_id` ))
																	LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_hr_pay` `rek` ON ( `rek`.`sys_ttrs_employee_id` = `ee`.`id` ))
																LEFT JOIN `db_klola`.`sys_tmst_bank` `bank` ON ( `bank`.`id` = `rek`.`sys_tmst_bank_id` ))
															LEFT JOIN `db_klola`.`sys_tmst_alasan_phk` `phk` ON ( `phk`.`id` = `ee`.`resign_reason` ))
														LEFT JOIN `db_klola`.`sys_tmst_province` `lvprov` ON ( `lvprov`.`id` = `ee`.`lv_sys_tmst_province_id` ))
													LEFT JOIN `db_klola`.`sys_tmst_regency` `lvgency` ON ( `lvgency`.`id` = `ee`.`lv_sys_tmst_regency_id` ))
												LEFT JOIN `db_klola`.`sys_tmst_district` `lvdist` ON ( `lvdist`.`id` = `ee`.`lv_sys_tmst_district_id` ))
											LEFT JOIN `db_klola`.`sys_tmst_province` `idprov` ON ( `idprov`.`id` = `ee`.`id_sys_tmst_province_id` ))
										LEFT JOIN `db_klola`.`sys_tmst_regency` `idgency` ON ( `idgency`.`id` = `ee`.`id_sys_tmst_regency_id` ))
									LEFT JOIN `db_klola`.`sys_tmst_district` `iddist` ON ( `iddist`.`id` = `ee`.`id_sys_tmst_district_id` ))
								LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_last_education` `edu` ON ( `edu`.`sys_ttrs_employee_id` = `ee`.`id` ))
							LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_mother_name` `fam` ON ( `fam`.`sys_ttrs_employee_id` = `ee`.`id` ))
						LEFT JOIN `db_klola_dvl`.`sys_tmst_religion` `rel` ON ( `rel`.`id` = `ee`.`sys_tmst_religion_id` ))
					LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_permanent_date` `pd` ON ( `pd`.`id` = `ee`.`id` ))
			LEFT JOIN `db_klola_dvl`.`sys_vtrpt_employee_database_maxstartdate` `ms` ON ( `ms`.`sys_ttrs_employee_id` = `ee`.`id` )) 
		WHERE
			1 = 1 
			AND `ee`.`employee_status` <> 0 
		GROUP BY
			`ee`.`id`,
			`ee`.`sys_tmst_employee_type_id` UNION ALL
		SELECT
			`e`.`nip` AS `id`,
			`e`.`firstname` AS `nama`,
			`e`.`custom11` AS `nama_depan`,
			`e`.`custom12` AS `nama_tengah`,
			`e`.`custom13` AS `nama_belakang`,
			`e`.`surname` AS `nama_panggilan`,
			'' AS `reporting_line_id`,
			'' AS `reporting_line_name`,
			`sd`.`name` AS `divisi`,
			`sd1`.`name` AS `departemen`,
		CASE
				
				WHEN substr( `de`.`departement_id`, 1, 9 ) THEN
				`de`.`name` ELSE '' 
			END AS `seksi`,
			`oc`.`alias` AS `jabatan`,
			`e`.`sys_tmst_cost_center_id` AS `kode_cost_center`,
			`cc`.`name` AS `nama_cost_center`,
			`un`.`name` AS `area_kerja`,
			`ec2`.`name` AS `spv_level`,
			`lev`.`name` AS `golongan`,
			`gra`.`name` AS `pangkat`,
			concat( `ec1`.`code`, ' - ', `ec1`.`name` ) AS `kota_recruitmen`,
			`ec7`.`description` AS `lokasi_kerja`,
			`e`.`startwork` AS `tgl_mulai_kerja`,
			`pd`.`permanent_date` AS `tgl_pegawai_tetap`,
			'' AS `tgl_efektif`,
			`p`.`not_active_work_date` AS `tgl_resign`,
		CASE
				
				WHEN octet_length( `phk`.`name` ) > 1 THEN
				`phk`.`name` ELSE `ph`.`alasan_keluar` 
			END AS `alasan_keluar`,
		CASE
				
				WHEN `e`.`employee_status` = '1' THEN
				'Percobaan' 
				WHEN `e`.`employee_status` = '2' THEN
				'Kontrak' 
				WHEN `e`.`employee_status` = '3' THEN
				'Tetap' 
				WHEN `e`.`employee_status` = '4' THEN
			CASE
					
					WHEN `e`.`last_employee_status` = '1' THEN
					'Percobaan' 
					WHEN `e`.`last_employee_status` = '2' THEN
					'Kontrak' 
					WHEN `e`.`last_employee_status` = '3' THEN
					'Tetap' 
				END 
				END AS `status_pegawai`,
				'Non Aktif' AS `status_aktif/nonaktif`,
				`edu`.`name` AS `pendidikan_terakhir`,
				`e`.`placeofborn` AS `tempat_lahir`,
				`e`.`dateofborn` AS `tanggal_lahir`,
			CASE
					
					WHEN `e`.`sex` = '1' THEN
					'L' ELSE 'P' 
				END AS `jenis_kelamin`,
				`mar`.`name` AS `status_perkawinan`,
				`e`.`married_date` AS `tanggal_pernikahan`,
			CASE
					
					WHEN `e`.`sys_tmst_country_id` = '360' THEN
					'WNI' ELSE 'WNA' 
				END AS `kewarganegaraan`,
				`typeid`.`name` AS `jenis_identitas`,
				`e`.`identity_number` AS `nomor_identitas`,
			CASE
					
					WHEN `e`.`custom14` = '1' THEN
					'_SEUMURHIDUP' ELSE `e`.`custom5` 
				END AS `kadaluarsa_identitas`,
				`e`.`kk_num` AS `nomor_kk`,
				`e`.`custom3` AS `nik_kk`,
				`e`.`custom6` AS `tanggal_dikeluarkan_KK`,
				`e`.`tax_npwp_personal` AS `npwp_pegawai`,
				`e`.`jamsostek_code` AS `nomor_jamsostek`,
				`e`.`bpjs_num` AS `nomor_bpjs_kesehatan`,
				`e`.`marital_status_tax` AS `kawin_pajak`,
				`kpp`.`name` AS `kpp`,
				`cnpp`.`name` AS `npp_perusahaan`,
				`cnpwp`.`npwp_number` AS `npwp_perusahaan`,
				`e`.`couple_name` AS `nama_pasangan`,
				`e`.`tax_npwp_couple` AS `npwp_pasangan`,
				`co`.`name` AS `negara`,
				`e`.`startincountry` AS `tgl_ke_indonesia`,
				`fam`.`name` AS `nama_ibu_kandung`,
				`blood`.`name` AS `golongan_darah`,
				`rel`.`name` AS `agama`,
				`e`.`weight_body` AS `berat_badan`,
				`e`.`tall_body` AS `tinggi_badan`,
				`lvprov`.`name` AS `domisili_propinsi`,
				`lvgency`.`name` AS `domisili_kabupaten`,
				`lvdist`.`name` AS `domisili_kecamatan`,
				`e`.`lv_kelurahan` AS `domisili_kelurahan`,
				`e`.`lv_rt` AS `domisili_rt`,
				`e`.`lv_rw` AS `domisili_rw`,
				`e`.`lv_address` AS `domisili_alamat`,
				`e`.`lv_postcode` AS `domisili_kodepos`,
				`idprov`.`name` AS `propinsi`,
				`idgency`.`name` AS `kabupaten`,
				`iddist`.`name` AS `kecamatan`,
				`e`.`id_kelurahan` AS `kelurahan`,
				`e`.`id_rt` AS `rt`,
				`e`.`id_rw` AS `rw`,
				`e`.`id_address` AS `alamat`,
				`e`.`id_postcode` AS `kodepos`,
				`e`.`custom4` AS `alamat_npwp`,
				`e`.`homephone` AS `telepon_rumah`,
				`e`.`officephone` AS `telepon_kantor`,
				`e`.`mobile1` AS `hp1`,
				`e`.`mobile2` AS `hp2`,
				'' AS `email_kantor`,
				'' AS `username`,
				`e`.`mail_personal` AS `email_pribadi`,
				`e`.`custom9` AS `faskes_umum`,
				`e`.`custom10` AS `faskes_gigi`,
				`rek`.`sys_tmst_bank_id` AS `kode_bank`,
				`bank`.`name` AS `nama_bank`,
				`rek`.`account_no` AS `nomor_rekening`,
				`rek`.`account_name` AS `atas_nama_rekening`,
				`rek`.`branch` AS `cabang`,
				`e`.`journal_code` AS `kode_jurnal`,
				`e`.`fb` AS `fb`,
				`e`.`ym` AS `ym`,
				`e`.`whatsup` AS `wa`,
				`e`.`twitter` AS `tw`,
				`e`.`blog` AS `blog`,
				`e`.`hangout` AS `hangout`,
				`e`.`hobby` AS `hobby`,
				'' AS `passport_num`,
				'' AS `passport_expired`,
				`e`.`sys_tmst_unit_id` AS `sys_tmst_unit_id`,
				`e`.`sys_tmst_grade_id` AS `sys_tmst_grade_id`,
				`e`.`sys_tmst_level_id` AS `sys_tmst_level_id`,
				`e`.`sys_tmst_company_id` AS `sys_tmst_company_id`,
				`e`.`sys_tmst_country_id` AS `sys_tmst_country_id`,
				`e`.`sys_ttrs_employee_id` AS `sys_ttrs_employee_id` 
			FROM
				(((((((((((((((((((((((((((((((((((
																																							`db_klola_dvl`.`sys_ttrs_employee_history` `e`
																																							JOIN `db_klola_dvl`.`sys_vtrpt_employee_database_rehire` `p` ON ( `p`.`sys_ttrs_employee_id` = `e`.`sys_ttrs_employee_id` ))
																																						LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom2` `ec` ON ( `e`.`custom2` = `ec`.`id` ))
																																					LEFT JOIN `db_klola_dvl`.`sys_tmst_unit` `un` ON ( `e`.`sys_tmst_unit_id` = `un`.`id` ))
																																				LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom1` `ec1` ON ( `e`.`custom1` = `ec1`.`id` ))
																																			LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom2` `ec2` ON ( `e`.`custom2` = `ec2`.`id` ))
																																		LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_custom7` `ec7` ON ( `e`.`custom7` = `ec7`.`id` ))
																																	LEFT JOIN `db_klola_dvl`.`sys_tmst_level` `lev` ON ( `e`.`sys_tmst_level_id` = `lev`.`id` ))
																																LEFT JOIN `db_klola_dvl`.`sys_tmst_grade` `gra` ON ( `e`.`sys_tmst_grade_id` = `gra`.`id` ))
																															LEFT JOIN `db_klola`.`sys_tmst_country` `co` ON ( `e`.`sys_tmst_country_id` = `co`.`id` ))
																														LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `de` ON ( `e`.`sys_tmst_department_id` = `de`.`id` ))
																													LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `sd` ON (
																													`sd`.`departement_id` = substr( `de`.`departement_id`, 1, 2 )))
																												LEFT JOIN `db_klola_dvl`.`sys_tmst_department` `sd1` ON (
																												`sd1`.`departement_id` = substr( `de`.`departement_id`, 1, 5 )))
																											LEFT JOIN `db_klola_dvl`.`sys_tmst_occupation` `oc` ON ( `e`.`sys_tmst_occupation_id` = `oc`.`id` ))
																										LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_upline_detail` `ud` ON ( `ud`.`id` = `e`.`id` ))
																									LEFT JOIN `db_klola_dvl`.`sys_tmst_cost_center` `cc` ON ( `cc`.`code` = `e`.`sys_tmst_cost_center_id` ))
																								LEFT JOIN `db_klola`.`sys_tmst_marital_status` `mar` ON ( `mar`.`id` = `e`.`sys_tmst_marital_status_id` ))
																							LEFT JOIN `db_klola_dvl`.`sys_tmst_identity_type` `typeid` ON ( `typeid`.`id` = `e`.`sys_tmst_identity_type_id` ))
																						LEFT JOIN `db_klola`.`sys_tmst_kpp_code` `kpp` ON ( `kpp`.`id` = `e`.`sys_tmst_kpp_id` ))
																					LEFT JOIN `db_klola_dvl`.`sys_tmst_company_npwp` `cnpwp` ON ( `cnpwp`.`id` = `e`.`sys_tmst_company_npwp_id` ))
																				LEFT JOIN `db_klola_dvl`.`sys_tmst_company_npp` `cnpp` ON ( `cnpp`.`id` = `e`.`sys_tmst_company_npp_id` ))
																			LEFT JOIN `db_klola_dvl`.`sys_tmst_blood_type` `blood` ON ( `blood`.`id` = `e`.`sys_tmst_blood_type_id` ))
																		LEFT JOIN `db_klola_dvl`.`sys_ttrs_employee_hr_pay` `rek` ON ( `rek`.`sys_ttrs_employee_id` = `e`.`sys_ttrs_employee_id` ))
																	LEFT JOIN `db_klola`.`sys_tmst_bank` `bank` ON ( `bank`.`id` = `rek`.`sys_tmst_bank_id` ))
																LEFT JOIN `db_klola`.`sys_tmst_alasan_phk` `phk` ON ( `phk`.`id` = `e`.`resign_reason` ))
															LEFT JOIN `db_klola`.`sys_tmst_province` `lvprov` ON ( `lvprov`.`id` = `e`.`lv_sys_tmst_province_id` ))
														LEFT JOIN `db_klola`.`sys_tmst_regency` `lvgency` ON ( `lvgency`.`id` = `e`.`lv_sys_tmst_regency_id` ))
													LEFT JOIN `db_klola`.`sys_tmst_district` `lvdist` ON ( `lvdist`.`id` = `e`.`lv_sys_tmst_district_id` ))
												LEFT JOIN `db_klola`.`sys_tmst_province` `idprov` ON ( `idprov`.`id` = `e`.`id_sys_tmst_province_id` ))
											LEFT JOIN `db_klola`.`sys_tmst_regency` `idgency` ON ( `idgency`.`id` = `e`.`id_sys_tmst_regency_id` ))
										LEFT JOIN `db_klola`.`sys_tmst_district` `iddist` ON ( `iddist`.`id` = `e`.`id_sys_tmst_district_id` ))
									LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_last_education` `edu` ON ( `edu`.`sys_ttrs_employee_id` = `e`.`sys_ttrs_employee_id` ))
								LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_mother_name` `fam` ON ( `fam`.`sys_ttrs_employee_id` = `e`.`sys_ttrs_employee_id` ))
							LEFT JOIN `db_klola_dvl`.`sys_tmst_religion` `rel` ON ( `rel`.`id` = `e`.`sys_tmst_religion_id` ))
						LEFT JOIN `db_klola_dvl`.`sys_vtrs_employee_permanent_date` `pd` ON ( `pd`.`id` = `e`.`sys_ttrs_employee_id` ))
				LEFT JOIN `db_klola_dvl`.`sys_vtrpt_employee_database_phk` `ph` ON ( `ph`.`sys_ttrs_employee_id` = `e`.`sys_ttrs_employee_id` AND `ph`.`createdate` = `e`.`createdate` )) 
			WHERE
				1 = 1 
				AND `e`.`modname` = 'trs_employee_phk' 
			GROUP BY
				`e`.`nip` 
			) `e` 
		GROUP BY
			`e`.`id` 
	ORDER BY
	`e`.`nama`